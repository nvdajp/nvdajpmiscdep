# mecabHarness.py
# -*- coding: utf-8 -*-
# Japanese text processor test module
# by Takuya Nishimoto

from __future__ import unicode_literals

# tasks: 要素2は音声合成の読み、(もしあれば)要素3は点訳用のカナ表記
# 点訳の表記と分かち書きは、規則で処理できないものを
# Mecab 辞書の第13フィールドに追加している。
# 要素3のスラッシュは形態素の区切り、スペースは形態素内のマスアケ

# some examples from nvdajp-jtalk-dic:
# http://nvdajp-jtalk-dic.heroku.com/items/
tasks = [
	['あ⣿あ∫あ♪',  'アイチニーサンヨンゴーロクナナハチノテンアセキブンキゴーアオンプ'],
	['鈹噯呃瘂蹻脘鑱涿癃',  'ヒアイアクアキョーカンザンタクリュー'],
	['十五絡脈病証',  'ジューゴカラマミャクヤマイアカシ'],
	['マーク。まーく。',  'マーク。マーク。'],
	['1 2',  'イチ ニ'],

	['行',  'ギョー'],
	#['一行', 'イチギョー'],
	['1行', 'イチギョー'],
	['１行', 'イチギョー'],
	['１行下', 'イチギョーシタ'],
	['１行上', 'イチギョーウエ'],
	['２行', 'ニギョー'],
	['３行', 'サンギョー'],
	['現在行', 'ゲンザイギョー'],
	['最上行', 'サイジョーギョー'],
	['1行下',  'イチギョーシタ'],
	['1行上',  'イチギョーウエ'],
	['誤判定', 'ゴハンテイ'],
	['50音順', 'ゴジューオンジュン'],
	['税', 'ゼイ'],
	['三毛猫', 'ミケネコ'],
	['表計算', 'ヒョーケイサン'],
	['小文字', 'コモジ'],
	['大文字', 'オーモジ'],
	['拡張子', 'カクチョーシ'],
	['世界中', 'セカイジュー'],
	['孫正義', 'ソンマサヨシ'],
	['2分前', 'ニフンマエ'],
	['２分前', 'ニフンマエ'],
	['障がい', 'ショーガイ'],
	['親オブジェクト', 'オヤオブジェクト'],
	['ｚ', 'ゼット'],
	['規', 'タダシ'],
	['全', 'ゼン'],
	['007', 'ゼロゼロナナ'],
	['上矢印', 'ウエヤジルシ'],
	['下矢印', 'シタヤジルシ'],
	['大見出し', 'オオミダシ'],
	['前景色', 'ゼンケイショク'],
	['梅雨前線', 'バイウゼンセン', 'バイウ ゼンセン'], 
	['八ッ場ダム', 'ヤンバダム'],
	['１都５県', 'イットゴケン'],
	['1都5県', 'イットゴケン'],
	['１都６県', 'イットロッケン'],
	['1都6県', 'イットロッケン'],
	['孫正義', 'ソンマサヨシ', 'ソン マサヨシ'], 
	['きゃりーぱみゅぱみゅ', 'キャリーパミュパミュ'],
	['いひ', 'イヒ'], # http://sourceforge.jp/ticket/browse.php?group_id=4221&tid=30919
	['金', 'キン' ],
	['1月', 'イチガツ', '1ガツ'], # NVDA点字表示の誤り No.18
	['為おおせる', 'シオオセル'], # 点訳のてびき第3版 第2章 その1 1 5 No.63
	['凡そ', 'オオヨソ'], # 点訳のてびき第3版 第2章 その1 1 5 No.67
	['無花果', 'イチジク'], # 点訳のてびき第3版 第2章 その1 1 6 No.3
	['砂利道', 'ジャリミチ'], # 点訳のてびき第3版 第2章 その1 1 6 No.10
	['少しずつ', 'スコシズツ'], # 点訳のてびき第3版 第2章 その1 1 6 No.20
	['鼓', 'ツヅミ'], # 点訳のてびき第3版 第2章 その1 1 6 No.32
	['葛籠', 'ツヅラ'], # 地名？ 点訳のてびき第3版 第2章 その1 1 6 No.33
	['紅提灯', 'ベニヂョーチン'], # 点訳のてびき第3版 第2章 その1 1 6 No.39
	['まづ', 'マズ'], # 点訳のてびき第3版 第2章 その1 1 7 No.1
	['一つづつ', 'ヒトツズツ'], # 点訳のてびき第3版 第2章 その1 1 7 No.2
	['大きう', 'オオキュー'], # 点訳のてびき第3版 第2章 その1 1 7 No.5
	['うれしう', 'ウレシュー'], # 点訳のてびき第3版 第2章 その1 1 7 No.6
	['もみぢ', 'モミジ'], # 点訳のてびき第3版 第2章 その1 1 7 No.7
	['みづうみ', 'ミズウミ'], # 点訳のてびき第3版 第2章 その1 1 7 No.8
	['ヴァイオリン', 'バイオリン' , 'ヴァイオリン'], # 点訳のてびき第3版 第2章 その1 2 1 No.4
	['ヴィタミン', 'ビタミン', 'ヴィタミン'], # 点訳のてびき第3版 第2章 その1 2 1 No.6
	['ラヂオ', 'ラジオ'], # 点訳のてびき第3版 第2章 その1 2 1 No.22
	['ヂャケット', 'ジャケット'], # 点訳のてびき第3版 第2章 その1 2 1 No.23
	['ウヰスキー', 'ウイスキー'], # 点訳のてびき第3版 第2章 その1 2 1 No.25
	['スヰフト', 'スイフト'], # 点訳のてびき第3版 第2章 その1 2 1 No.26
	['ヱルテル', 'ウェルテル'], # 点訳のてびき第3版 第2章 その1 2 1 No.27
	['ヲルポール', 'ウォルポール'], # 点訳のてびき第3版 第2章 その1 2 1 No.28
	['ヘリコプタア', 'ヘリコプター'], # 点訳のてびき第3版 第2章 その1 2 1 No.29
	['ちゅうりっぷ', 'チューリップ'], # 点訳のてびき第3版 第2章 その1 2 1 No.30
	['おおきに', 'オーキニ',  'オオキニ', ], # 点訳のてびき第3版 第2章 その1 2 3 No.4
	['おみやぁさん', 'オミャアサン', ], # 点訳のてびき第3版 第2章 その1 2 3 No.6
	['先生ぇさまぁ', 'センセエサマア', ], # 点訳のてびき第3版 第2章 その1 2 3 No.7
	['おとゥ', 'オトー', ], # 点訳のてびき第3版 第2章 その1 2 4 No.1
	['おかァ', 'オカア', ], # 点訳のてびき第3版 第2章 その1 2 4 No.1
	['クヮルテット', 'クァルテット', ], # 点訳のてびき第3版 第2章 その1 2 4 No.5
	['ヂェスチャー', 'ジェスチャー', ], # 点訳のてびき第3版 第2章 その1 2 4 No.6
	['スェーター', 'スエーター', ], # 点訳のてびき第3版 第2章 その1 2 4 No.7
	['ヒァーッ', 'ヒャーッ', ], # 点訳のてびき第3版 第2章 その1 2 4 No.8
	['大阪', 'オーサカ', 'オオサカ'], # 点訳のてびき第3版 第2章 その1 2 5 No.1
	['遠野', 'トーノ', 'トオノ' ], # 点訳のてびき第3版 第2章 その1 2 5 No.2
	['東井', 'トーイ', ], # 点訳のてびき第3版 第2章 その1 2 5 No.3
	['青梅', 'オーメ', ], # 点訳のてびき第3版 第2章 その1 2 5 No.10
	['透', 'トール', 'トオル', ], # 点訳のてびき第3版 第2章 その1 2 5 No.11
	['みさを', 'ミサオ', 'ミサヲ', ], # 点訳のてびき第3版 第2章 その1 2 5 No.13
	['かほる', 'カオル', 'カホル', ], # 点訳のてびき第3版 第2章 その1 2 5 No.14
	['さをり', 'サオリ', 'サヲリ', ], # 点訳のてびき第3版 第2章 その1 2 5 No.19
	['みやこをどり', 'ミヤコオドリ', 'ミヤコ オドリ', ], # 点訳のてびき第3版 第2章 その1 2 5 No.24
	['をりがみ', 'オリガミ', ], # 点訳のてびき第3版 第2章 その1 2 5 No.25
	['八幡平', 'ハチマンタイ', ], # ヤワタダイラ
	['山ん中', 'ヤマンナカ', ], # 点訳のてびき第3版 第3章 その1 2 No.14
	['そういうわけ', 'ソーユウワケ', 'ソー イウ/ワケ', ], # 点訳のてびき第3版 第3章 その1 3 No.51
	['そういう', 'ソーユウ', 'ソー イウ', ], # 点訳のてびき第3版 第3章 その1 5 No.2
	['どうして', 'ドーシテ', 'ドー シテ', ], # 点訳のてびき第3版 第3章 その1 5 No.5

	['システムキャレット', None, 'システム/キャレット'],
	['フィードバック', None, 'フィード バック'],
	['インターフェース', None, 'インター フェース'],
	['オペレーティングシステム', None, 'オペレーティング システム'],
	['トーキングインストーラー', None, 'トーキング/インストーラー'],
	['アイスクリーム', None, 'アイス クリーム'],
	['日本点字図書館', None, 'ニッポン テンジ トショカン'],

	['通り', None, 'トオリ'],
	['狼', None, 'オオカミ'],
	['通る', None, 'トオル'],
	['多い', None, 'オオイ'],
	['多く', None, 'オオク'],
	['大晦日', None, 'オオミソカ'],
	['手作り', None, 'テヅクリ'],
	['南半球', None, 'ミナミ ハンキュー'],
	['アメリカ合衆国', None, 'アメリカ ガッシューコク'],
	['第一人者', None, 'ダイ1ニンシャ'],
	['一流', None, '1リュー'],
	['一月', None, '1ガツ'],
	['二月', None, '2ガツ'],
	['四方', None, '4ホー'],
	['六法全書', None, '6ポー ゼンショ'],
	['百人一首', None, '100ニン 1シュ'],
	['ヱビスビール', None, 'エビス ビール'],
	['日本コロムビア', None, 'ニッポン コロムビア'],
	['ビタミンＥ', None, 'ビタミン E'],
	['劇団四季', None, 'ゲキダン 4キ'],
	['四季', None, '4キ'],
	['四半期', None, '4ハンキ'],
	['四角形', None, '4カクケイ'],
	['四条', None, '4ジョー'],
	['二男', None, '2ナン'],
	['十数', None, 'ジュー/スー'], # 十,名詞,数
	['一輪車', None, '1リンシャ'],
	['三塁打', None, '3ルイダ'],
	['一汁一菜', None, '1ジュー 1サイ'],
	['五臓六腑', None, '5ゾー 6プ'],
	['二・二六事件', None, '2⠼26 ジケン'],
	['一段', None, '1ダン'],
	['七転び八起き', None, 'ナナコロビ ヤオキ'],
	['十重二十重', None, 'トエ ハタエ'],
	['３ラン', None, '3ラン'],
	['さんりんしゃ', None, '3リンシャ'],
	['いちばん', None, '1バン'],
	['Ｘ線', None, 'Xセン'],

	{'text':'カムイトﾟラノ', 'speech':'カムイトラノ'}, # U+ff9f
	{'text':'カムイト゚ラノ', 'speech':'カムイトラノ'}, # U+309a
	{'text':'カムイト゜ラノ', 'speech':'カムイトラノ'}, # U+309c

	{'text':'触読', 'braille':'ショクドク'},
	{'text':'触読式時計', 'braille':'ショクドクシキ トケイ'},
	{'text':'触手話', 'braille':'ショクシュワ'},
	{'text':'触読手話', 'braille':'ショクドク/シュワ'},
	{'text':'盲ろう', 'braille':'モーロー'},

	{'text':'泉質', 'braille':'センシツ'},
	{'text':'硫酸塩', 'braille':'リューサンエン'},
	{'text':'硫酸塩泉', 'braille':'リューサンエンセン'},
	{'text':'塩化物泉', 'braille':'エンカブッセン'},
	{'text':'泉温', 'braille':'センオン'},
	{'text':'冷鉱泉', 'braille':'レイコーセン'},
	{'text':'微温泉', 'braille':'ビオンセン'},
	{'text':'療養泉', 'braille':'リョーヨーセン'},
	{'text':'低張性', 'braille':'テイチョーセイ'},
	{'text':'等張性', 'braille':'トーチョーセイ'},
	{'text':'高張性', 'braille':'コーチョーセイ'},
	{'text':'酸性泉', 'braille':'サンセイセン'},
	{'text':'放射能泉', 'braille':'ホーシャノーセン'},

	{'text':'次章', 'braille':'ジショー'},
	{'text':'お土産を有難うございます', 'braille':'オ/ミヤゲ/ヲ/アリガトー/ゴザイ/マス'},

	#['好かんやつ', 'スカンヤツ', 'スカン ヤツ', ], #点訳のてびき第3版 第3章 その1 2 No.16
	#['嘘みたいな話', 'ウソミタイナハナシ', 'ウソミタイナ ハナシ', ], # 点訳のてびき第3版 第3章 その1 2 No.17
	#['行っていらっしゃい', 'イッテイラッシャイ', 'イッテ イラッシャイ', ], # 点訳のてびき第3版 第3章 その1 4 No.77
	['ごめんください', 'ゴメンクダサイ', 'ゴメン クダサイ', ], # 点訳のてびき第3版 第3章 その1 4 No.84
	['おはようございます', 'オハヨーゴザイマス', 'オハヨー ゴザイマス', ], # 点訳のてびき第3版 第3章 その1 4 No.86

	# 点字表記辞典「あ」(1)
	['相対する', 'アイタイスル'],
	['相対的', 'ソータイテキ'], # regression test
	['相たずさえて', 'アイタズサエテ'],
	['相整う', 'アイトトノウ'],
	['同病相憐れむ', 'ドービョーアイアワレム', 'ドービョー/アイアワレム'],
	['木立の間から見える', 'コダチノアイダカラミエル'],
	['開いた口がふさがらぬ', 'アイタクチガフサガラヌ'],
	['相無く涙ぐみ足り', 'アイナクナミダグミタリ'],
	['お生憎様', 'オアイニクサマ'],
	['開かずの間', 'アカズノマ'],
	['不開の間', 'アカズノマ'],
	['山田県主', 'ヤマダノアガタヌシ'],
	['暁闇', 'アカツキヤミ'],
	['上がり降り', 'アガリオリ'],
	['河原町四条上ル', 'カワラマチシジョーアガル'],
	['秋津国', 'アキツクニ'],
	['現つ神', 'アキツカミ'],
	['阿Ｑ正伝', 'アキューセイデン'],
	['悪源太', 'アクゲンタ'],
	['明くる朝', 'アクルアサ', 'アクル アサ'],
	['明くる年', 'アクルトシ', 'アクル トシ'],
	['明の星', 'アケノホシ', 'アケノ ホシ'],
	['麻布十番', 'アザブジューバン', 'アザブ ジューバン'],
	['男漁り', 'オトコアサリ', 'オトコ アサリ'],
	['古本漁り', 'フルホンアサリ', 'フルホン アサリ'],
	['足の甲', 'アシノコー', 'アシノ コー'],
	['日の足が伸びる', 'ヒノアシガノビル', 'ヒノアシ/ガ/ノビル'],
	{'text':'醤油味', 'braille':'ショーユ アジ'},
	{'text':'砂糖醤油', 'speech':'サトージョウユ'}, # regression test

	# 点字表記辞典「あ」(2)
	{'text':'足手纏い', 'braille':'アシデ マトイ'},
	{'text':'手荷物預かり所', 'braille':'テニモツ/アズカリ/ショ'}, # ジョ
	#{'text':'額に汗して', 'braille':'ヒタイニ アセ シテ'},
	#{'text':'認め遊ばす', 'braille':'シタタメアソバス'},
	{'text':'源朝臣頼政', 'braille':'ミナモトノ アソン ヨリマサ'},
	{'text':'東漢直駒', 'braille':'ヤマトノ アヤノ アタイノ コマ'},
	#{'text':'徒し男', 'braille':'アダシ オトコ'},
	#{'text':'徒し世', 'braille':'アダシヨ'},
	#{'text':'新し物好き', 'braille':'アタラシモノズキ'},
	#{'text':'暖かご飯', 'braille':'アッタカ ゴハン'},
	#{'text':'城跡', 'braille':'シロアト'},
	#{'text':'兄妹', 'braille':'アニ イモート'},
	{'text':'兄貴風を吹かす', 'input':'アニキカゼヲ フカス', 'braille':'アニキカゼ/ヲ/フカス'},
	#{'text':'秀兄イ', 'braille':'ヒデ アニイ'},

	{'text':'一日から十日', 'braille':'イチ/ニチ/カラ/ジュー/ニチ'},
	{'text':'1日から10日', 'braille':'イチ/ニチ/カラ/イチ/ゼロ/ニチ'},
	{'text':'一日から一〇日', 'braille':'イチ/ニチ/カラ/イチ/レイ/ニチ'},
	{'text':'名speech集', 'speech':'メイスピーチシュー', 'braille':'メイspeechシュー'},
	{'text':'一人当り10個ずつ', 'braille':'ヒトリアタリ/イチ/ゼロ/コ/ズツ'},
	{'text':'０４月', 'speech':'ゼロシガツ', 'braille':'ゼロ/4ガツ'},

	# 2013-08-25

	['manage', 'マネイジ'],
	['choose', 'チュウズ'],
	['impaired', 'インペアド'],
	['failed', 'フェイルド'],
	['mixi', 'ミクシー'],
	['nullsoft', 'ヌルソフト'],
	['speech api', 'スピーチ エーピーアイ'],
	['use', 'ユース'],
	['echo', 'エコウ'],
	['you', 'ユー'],
	['youtube', 'ユーチューブ'],
	['files', 'ファイルズ'],
	['docs', 'ドックス'],
	['labs', 'ラブス'],
	['japan', 'ジャパン'],
	['japanese', 'ジャパニーズ'],
	['favorites', 'フェイバリッツ'],
	['documents', 'ドキュメンツ'],
	['settings', 'セッティングズ'],
	['one', 'ワン'],
	#['onegai', 'オネーガイー'],
	['redistributable', 'リディストリビュータブル'],
	['app', 'アップ'],
	['types', 'タイプス'],
	['mouse', 'マウス'],
	['page', 'ページ'],

	{'text':'usage', 'speech':'ユーセイジ'},
	{'text':'june', 'speech':'ジューン'},
	{'text':'foundation', 'speech':'ファウンデイション'},
	{'text':'everyone', 'speech':'エブリワン'},
	{'text':'allowed', 'speech':'アラウド'},
	{'text':'designed', 'speech':'デザインド'},
	{'text':'take', 'speech':'テイク'},
	{'text':'share', 'speech':'シェアー'},
	{'text':'change', 'speech':'チェインジ'},
	{'text':'guarantee', 'speech':'ギャランティー'},
	{'text':'preamble', 'speech':'プリアムブル'},
	{'text':'make', 'speech':'メイク'},
	{'text':'sure', 'speech':'シュア'},
	{'text':'users', 'speech':'ユーザーズ'},
	{'text':'some', 'speech':'サム'},
	#{'text':'★～＜QUIZ＞～～～～～～～～～～～～～～～', 'speech':'★～＜ＱＵＩＺ＞～～～～～～～～～～～～～～～', 'braille':'★/～/＜/ＱＵＩＺ/＞/～/～/～/～/～/～/～/～/～/～/～/～/～/～/～'},
	{'text':'database', 'speech':'データベース'},
	{'text':'mode', 'speech':'モウドゥ'},
	{'text':'opened', 'speech':'オープンド'},
	{'text':'closed', 'speech':'クローズド'},
	{'text':'unknown', 'speech':'アンノウン'},
	{'text':'button', 'speech':'ボタン'},
	{'text':'animation', 'speech':'アニメイション'},
	{'text':'date', 'speech':'デイトゥ'},
	{'text':'note', 'speech':'ノゥトゥ'},
	{'text':'layered', 'speech':'レイヤード'},
	{'text':'shape', 'speech':'シェイプ'},
	{'text':'size', 'speech':'サイズ'},
	{'text':'data', 'speech':'デイタ'},
	{'text':'required', 'speech':'リクワイアード'},
	{'text':'auto', 'speech':'オート'},
	{'text':'iconified', 'speech':'アイコニファイド'},
	{'text':'focusable', 'speech':'フォウカサブル'},
	{'text':'editable', 'speech':'エディタブル'},
	{'text':'draggable', 'speech':'ドゥラッガブル'},
	{'text':'contains', 'speech':'コンテインズ'},
	{'text':'errors', 'speech':'エラーズ'},
	{'text':'position', 'speech':'ポジション'},
	{'text':'previously', 'speech':'プリビアスリ'},
	{'text':'entered', 'speech':'エンタード'},
	{'text':'turns', 'speech':'ターンズ'},
	{'text':'toggles', 'speech':'トグルズ'},
	{'text':'clicks', 'speech':'クリックス'},
	{'text':'unlock', 'speech':'アンロック'},
	{'text':'unlocks', 'speech':'アンロックス'},
	{'text':'locks', 'speech':'ロックス'},
	{'text':'controls', 'speech':'コントゥロウルズ'},
	{'text':'currently', 'speech':'カレントリ'},
	{'text':'synth', 'speech':'シンセ'},
	{'text':'moves', 'speech':'ムーブズ'},
	{'text':'characters', 'speech':'キャラクターズ'},
	{'text':'keys', 'speech':'キーズ'},
	{'text':'cycles', 'speech':'サイクルズ'},
	{'text':'levels', 'speech':'レベルズ'},
	{'text':'speaks', 'speech':'スピークス'},

	{'text':'更衣室', 'braille':'コーイシツ'},
	{'text':'盗撮', 'braille':'トーサツ'},
	#{'text':'北の風晴れ所により一時雨', 'braille':'キタノ/カゼ/ハレ/トコロニ ヨリ/イチジ/アメ'},
	{'text':'晴れ所により一時雨', 'braille':'ハレ/トコロニ ヨリ/イチジ/アメ'},
	{'text':'山西', 'braille':'ヤマニシ'},
	{'text':'久代', 'braille':'ヒサヨ'},
	{'text':'田中梅木鈴木',  'braille':'タナカ/ウメキ/スズキ'},
	{'text':'田中　梅木　鈴木',  'speech':'タナカ ウメキ スズキ'},
	{'text':'山陽本線',  'speech':'サンヨーホンセン'},
	{'text':'山口県',  'speech':'ヤマグチケン'},

	{'text':'編集人', 'braille':'ヘンシューニン'},
	{'text':'発行人', 'braille':'ハッコーニン'},
	{'text':'受取人', 'braille':'ウケトリニン'},
	{'text':'配達人', 'braille':'ハイタツニン'},
	{'text':'管理人', 'braille':'カンリニン'},
	{'text':'下請人', 'braille':'シタウケニン'},

	{'text':'遠い向こう', 'speech':'トオイムコー', 'braille':'トオイ/ムコー'},
	#{'text':'とおいむこう', 'speech':'トオイムコー', 'braille':'トオイ/ムコー'},
	{'text':'あいうえお', 'braille':'アイウエオ'},
	{'text':'かきくけこ', 'braille':'カキクケコ'},

	{'text':'untitled', 'speech':'アンタイトルド'},
	{'text':'reopen', 'speech':'リオウプン'},
	{'text':'chain', 'speech':'チェイン'},
	{'text':'auto', 'speech':'オート'},
	{'text':'minute', 'speech':'ミニットゥ'},
	{'text':'flanger', 'speech':'フランジャー'},
	{'text':'stereo', 'speech':'ステリオウ'},
	{'text':'rate', 'speech':'レイトゥ'},
	{'text':'resample', 'speech':'リサンプル'},
	{'text':'exit', 'speech':'エグジットゥ'},
	{'text':'version', 'speech':'バージョン'},
	{'text':'epub', 'speech':'イーパブ'},
	{'text':'libre', 'speech':'リブレ'},
	{'text':'picture', 'speech':'ピクテュァ'},
	{'text':'gpu', 'speech':'ジーピーユー'},
	{'text':'drivers', 'speech':'ドゥライバーズ'},
	{'text':'unicode', 'speech':'ユニコウドゥ'},
	{'text':'base', 'speech':'ベイス'},
	{'text':'are', 'speech':'アー'},
	{'text':'visum', 'speech':'ビズム'},

	{'text':'ありがとう', 'braille':'アリガトー'},
	{'text':'有り難う', 'braille':'アリガトー'},
	{'text':'有り難うございました', 'braille':'アリガトー/ゴザイ/マシ/タ'},

	{'text':'展開', 'braille':'テンカイ'},
	{'text':'テンカイ', 'braille':'テンカイ'},

	{'text':'付点', 'speech':'フテン'},

	{'text':'来る３月には', 'speech':'キタルサンガツニワ'},
	#{'text':'来る途中で、', 'speech':'クルトチューデ、'},

	#{'text':'これらは正しくありません', 'speech':'コレラワタダシクアリマセン'},
	{'text':'正しくない', 'speech':'タダシクナイ'},
	#{'text':'正しく書け', 'speech':'タダシクカケ'},
	{'text':'正しく生きよう', 'speech':'タダシクイキヨウ'},
	{'text':'正しく奇跡だ', 'speech':'マサシクキセキダ'},
	{'text':'正しく神業だ', 'speech':'マサシクカミワザダ'},

	#{'text':'設定情報を初期値に戻しました', 'speech':'セッテイジョーホーヲショキチニモドシマシタ'},
]
